forga Ansible base tasks
========================


💡 Idea
-------

Get info and set basic config for _Debian_ and _Ubuntu_ distrib, using [_Gnome_](https://www.gnome.org/), [_Mate_](https://mate-desktop.org/) or no desktop environment.

Suitable for server and workstation.


✨ Features
-----------

|   task                                                |   purpose                                                             |
|   :--------------------------------------:            |   :--------------------------------------------------------------:    |
| [`apt.yml`](tasks/apt.yml)                            |   Install generic packages                                            |
| [`become_user_cfg.yml`](tasks/become_user_cfg.yml)    |   Set `sudo` without password for `become_user` access                |
| [`host_info.yml`](tasks/host_info.yml)                |   Return message with distribution full name & version                |
| [`shutdown.yml`](tasks/shutdown.yml)                  |   Shutdown target in 5 min (not if host is in production group)       |
| [`system_cfg`](tasks/system_cfg/main.yml)             |   Configure  `root`, firewall, NTP, LDP & `{{ my_users }}` shell      |
| [`user_cfg`](tasks/user_cfg/main.yml)                 |   Configure `{{ my_user }}`                                           |
| [`whoami.yml`](tasks/whoami.yml)                      |   Return message with `ansible_user` & `become_user` (`sudo` method)  |


🚀 Quickstart
-------------

1. Setup your `inventory` file from [`inventory.sample`](inventory.sample) :
    - `make inventory_generation`
1. Run `host_info` playbook to `<group_foo>` & `<group_bar>` :
    - `ansible-playbook tasks/host_info.yml -i inventory -e host_list=<group_foo>:<group_bar>`
