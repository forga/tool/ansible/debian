EDITOR = geany

clean:
	# Remove files not in source control
	find . -type f -name "*.retry" -delete
	find . -type f -name "*.orig" -delete

open_all:
	${EDITOR} .gitignore inv* Makefile README.md
	${EDITOR} tasks/*.yml
	${EDITOR} tasks/files/sshd_config
	${EDITOR} tasks/templates/*.j2
	${EDITOR} tasks/system_cfg/*.yml
	${EDITOR} tasks/system_cfg/templates/*.j2
	${EDITOR} tasks/user_cfg/*.yml
	${EDITOR} tasks/user_cfg/files/dotfiles/*
	${EDITOR} tasks/user_cfg/templates/*.j2
	${EDITOR} tasks/user_cfg/vars/*.yml
	${EDITOR} tasks/vars/*.yml

inventory_generation:
	cp inventory.sample inventory && ${EDITOR} inventory
